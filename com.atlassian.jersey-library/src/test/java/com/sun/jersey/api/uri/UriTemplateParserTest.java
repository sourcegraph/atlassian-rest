package com.sun.jersey.api.uri;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UriTemplateParserTest
{
    /**
     * Make sure that whatever version of Jersey we're using, it at least has the fix for
     * https://java.net/jira/browse/JERSEY-1198 (atlassian issue: https://ecosystem.atlassian.net/browse/REST-247 )
     */
    @Test
    public void canHaveRegularExpressionCharactersInTemplate()
    {
        UriTemplateParser parser = new UriTemplateParser(".^&!?-:<([$=)],>*+|");

        assertEquals("\\.\\^\\&\\!\\?\\-\\:\\<\\(\\[\\$\\=\\)\\]\\,\\>\\*\\+\\|", parser.getPattern().pattern());
    }
}
