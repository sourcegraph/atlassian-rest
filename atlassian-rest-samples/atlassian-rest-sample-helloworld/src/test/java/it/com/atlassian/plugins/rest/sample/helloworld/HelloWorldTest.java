package it.com.atlassian.plugins.rest.sample.helloworld;

import com.atlassian.plugins.rest.sample.helloworld.HelloWorld;
import com.atlassian.plugins.rest.sample.helloworld.UriBuilder;
import com.sun.jersey.api.client.Client;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.net.URI;

/**
 * Testing {@link HelloWorld}
 */
public class HelloWorldTest
{
    @Test
    public void testGetHelloWorld()
    {
        final URI baseUri = UriBuilder.create().path("rest").path("hw").path("1").build();
        final String hello = Client.create().resource(baseUri).path("hello").path("world").get(String.class);
        assertEquals("Hello world", hello);
    }
}
