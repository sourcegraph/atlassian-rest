package it.com.atlassian.plugins.rest.sample.entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import com.atlassian.plugins.rest.sample.entities.UriBuilder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DtdReadingTest
{
    public static final String FRUIT_RESOURCE = "fruit";
    private static final String BASKET_RESOURCE = "fruit-basket";
    HttpAttemptDetector detector;
    Thread t;

    @Before
    public void setupDetector() throws IOException
    {
        detector = new HttpAttemptDetector();
        t = new Thread(detector);
        t.start();
    }

    @After
    public void stopListenerThread() throws IOException
    {
        detector.shutdown();
    }

    PostMethod postResource(String resource, String content) throws IOException
    {
        RequestEntity re = new StringRequestEntity(content, "application/xml", "us-ascii");

        final URI baseUri = UriBuilder.create().path("rest").path("entity").path("latest").path(resource).build();

        HttpClient client = new HttpClient();

        PostMethod m = new PostMethod(baseUri.toString());

        m.setRequestHeader("Accept", "application/xml");
        m.setRequestEntity(re);

        client.executeMethod(m);

        return m;
    }

    @Test
    public void externalDtdIsNotRead() throws Exception
    {
        String content = "<!DOCTYPE x SYSTEM '" + detector.getUrl() + "'><orange><name>test</name></orange>";
        PostMethod m = postResource(FRUIT_RESOURCE, content);
        assertEquals(HttpStatus.SC_OK, m.getStatusCode());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalDtdIsNotReadWhenListParameter() throws Exception
    {
        String content = "<!DOCTYPE x SYSTEM '" + detector.getUrl() + "'><basket><orange><name>orange-one</name></orange><orange><name>orange-two</name></orange></basket>";
        PostMethod m = postResource(BASKET_RESOURCE, content);
        assertEquals(HttpStatus.SC_OK, m.getStatusCode());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalGeneralEntityIsNotRead() throws Exception
    {
        String content = "<!DOCTYPE x [<!ENTITY e SYSTEM '" + detector.getUrl() + "'>]><orange><name>&e;</name></orange>";
        PostMethod m = postResource(FRUIT_RESOURCE, content);
        assertEquals(HttpStatus.SC_OK, m.getStatusCode());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalGeneralEntityIsNotReadWhenListParameter() throws Exception
    {
        String content = "<!DOCTYPE x [<!ENTITY e SYSTEM '" + detector.getUrl() + "'>]><basket><orange><name>&e;</name></orange></basket>";
        PostMethod m = postResource(BASKET_RESOURCE, content);
        assertEquals(HttpStatus.SC_BAD_REQUEST, m.getStatusCode());  // entity is not defined because DTD is not read
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalParameterEntityIsNotRead() throws Exception
    {
        String content = "<!DOCTYPE x [<!ENTITY % pe SYSTEM '" + detector.getUrl() + "'> %pe;]><orange><name>test</name></orange>";
        PostMethod m = postResource(FRUIT_RESOURCE, content);
        assertEquals(HttpStatus.SC_OK, m.getStatusCode());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalParameterEntityIsNotReadWhenListParameter() throws Exception
    {
        String content = "<!DOCTYPE x [<!ENTITY % pe SYSTEM '" + detector.getUrl() + "'> %pe;]><basket><orange><name>test</name></orange></basket>";
        PostMethod m = postResource(BASKET_RESOURCE, content);
        assertEquals(HttpStatus.SC_OK, m.getStatusCode());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    /**
     * Listen on a socket, flag connection attempts.
     */
    static class HttpAttemptDetector implements Runnable
    {
        private final ServerSocket serverSocket;
        private final AtomicBoolean someoneConnected = new AtomicBoolean(false);

        public HttpAttemptDetector() throws IOException
        {
            serverSocket = new ServerSocket(0);
        }

        public String getUrl()
        {
            return "http://localhost:" + serverSocket.getLocalPort();
        }

        public void run()
        {
            while (true)
            {
                try
                {
                    Socket s = serverSocket.accept();
                    someoneConnected.set(true);

                    /* Pretend to be an HTTP server serving up an empty stream of bytes */
                    final InputStream in = s.getInputStream();
                    final BufferedReader br = new BufferedReader(new InputStreamReader(in, "us-ascii"));

                    String str;

                    while ((str = br.readLine()) != null && !str.isEmpty());

                    OutputStream out = s.getOutputStream();
                    out.write("HTTP/1.0 200 OK\r\n\r\n".getBytes("us-ascii"));
                    out.close();
                    s.close();
                }
                catch (IOException e)
                {
                }
            }
        }

        void shutdown() throws IOException
        {
            serverSocket.close();
        }

        public boolean wasAttempted()
        {
            return someoneConnected.get();
        }
    }
}
