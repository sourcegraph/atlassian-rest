package it.com.atlassian.plugins.rest.sample.entities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import javax.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.sample.entities.EntityClientServlet;
import com.atlassian.plugins.rest.sample.entities.EntityResource;
import com.atlassian.plugins.rest.sample.entities.UriBuilder;

import com.sun.jersey.api.client.Client;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import static com.atlassian.core.util.ClassLoaderUtils.getResourceAsStream;
import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.P_ACCEPT;
import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.P_CONTENT_TYPE;
import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.P_ORANGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Testing {@link EntityResource}
 */
public class EntityTest
{
    /* A secret that shouldn't be exposed to anonymous requests */
    private static final String SECRET = Long.toString(Double.doubleToLongBits(Math.random()));

    protected File createSecretFile() throws IOException
    {
        File f = File.createTempFile("secret-file", ".txt");
        FileUtils.write(f, SECRET);
        f.deleteOnExit();
        return f;
    }

    /**
     * Simple test that executes {@link EntityClientServlet}, which uses the {@link com.atlassian.sal.api.net.RequestFactory} to query
     * {@link EntityResource}
     */
    @Test
    public void testApplesForOranges()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri).queryParam(P_ORANGE, "valencia").get(String.class);
        assertEquals(
                "apple-valencia\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/xml", returned);   // application/xml is the default content-type & accept header value
    }

    @Test
    public void testApplesForOrangesJson()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/json")
                .queryParam(P_ACCEPT, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/json\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesDefaultContentTypeAcceptJson()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_ACCEPT, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesExplicitXmlContentTypeAndAccept()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/xml")
                .queryParam(P_ACCEPT, "application/xml")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/xml", returned);
    }

    @Test
    public void testApplesForOrangesMissingAcceptHeaderDefaultsToContentType()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/json\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesMultipleValidAcceptHeaders()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_ACCEPT, "application/json")
                .queryParam(P_ACCEPT, "application/xml")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" + //default
                        "Accept=application/json,application/xml", returned);
    }


    private PostMethod postToRestEndpoint(String content) throws IOException
    {
        RequestEntity re = new StringRequestEntity(content, "application/xml", "us-ascii");

        final URI baseUri = UriBuilder.create().path("rest").path("entity").path("latest").path("fruit").build();

        HttpClient client = new HttpClient();

        PostMethod m = new PostMethod(baseUri.toString());

        m.setRequestHeader("Accept", "application/xml");
        m.setRequestEntity(re);

        client.executeMethod(m);

        return m;
    }

    @Test
    public void testEntityExpansionDoesNotIncludeFileContents() throws IOException
    {
        InputStream in = getResourceAsStream("EntityTest-rest-include-external-entity.xml", this.getClass());
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        contents = contents.replace("/etc/passwd", createSecretFile().toURI().toString());

        PostMethod m = postToRestEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertEquals("The response should be XML",
                "application/xml",
                mt.getType() + '/' + mt.getSubtype()
        );
        assertThat(resp, CoreMatchers.not(JUnitMatchers.containsString(SECRET)));
    }

    @Test
    public void testValidEntitiesAreExpanded() throws IOException
    {
        InputStream in = getResourceAsStream("EntityTest-rest-with-amp-entity.xml", this.getClass());
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        PostMethod m = postToRestEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertEquals("The response should be XML",
                "application/xml",
                mt.getType() + '/' + mt.getSubtype()
        );
        assertThat(resp, JUnitMatchers.containsString("apple-&amp;"));
    }

    @Test
    public void testEntityExpansionDoesNotCauseDenialOfService() throws IOException
    {
        InputStream in = getResourceAsStream("EntityTest-rest-billion-laughs.xml", this.getClass());
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        PostMethod m = postToRestEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        // For this deployment this means a simple syntax error, in some environments you will actually get a OOME if this fails.
        assertThat("The response should not indicate a server memory error",
                resp, JUnitMatchers.containsString("The request sent by the client was syntactically incorrect"));

        assertThat("The response should not indicate a server memory error",
                resp, CoreMatchers.not(JUnitMatchers.containsString("java.lang.OutOfMemoryError")));
    }

    @Test
    public void fruitRetrievedAsXmlObeysJaxbAnnotations()
    {
        final URI baseUri = UriBuilder.create().path("rest/entity/1/fruit/jackfruit").build();
        final String returned = Client.create().resource(baseUri)
                .accept(MediaType.APPLICATION_XML)
                .get(String.class);

        assertThat(returned, Matchers.containsString("<jackFruit jaxb-description=\""));
    }

    @Test
    public void fruitRetrievedAsJsonObeysJacksonAnnotations()
    {
        final URI baseUri = UriBuilder.create().path("rest/entity/1/fruit/jackfruit").build();
        final String returned = Client.create().resource(baseUri)
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        assertThat(returned, Matchers.startsWith("{\"json-description\":\"fresh at"));
    }
}
