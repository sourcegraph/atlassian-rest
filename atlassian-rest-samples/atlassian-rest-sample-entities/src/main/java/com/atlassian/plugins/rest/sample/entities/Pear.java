package com.atlassian.plugins.rest.sample.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A simple Pear
 */
@XmlRootElement
public class Pear
{
    @XmlAttribute
    private String name;

    public Pear()
    {
    }

    public Pear(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
