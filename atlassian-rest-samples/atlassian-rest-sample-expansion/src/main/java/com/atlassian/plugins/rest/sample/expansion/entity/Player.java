package com.atlassian.plugins.rest.sample.expansion.entity;

import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.expand.Expandable;

import javax.ws.rs.core.UriBuilder;
import static javax.xml.bind.annotation.XmlAccessType.FIELD;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Details about a player and a link to their record if available.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
public class Player
{
    @XmlAttribute
    private String expand;

    @XmlAttribute
    private int id;

    @XmlElement
    private String fullName;

    @XmlElement
    private Link link;

    @Expandable
    @XmlElement
    private PlayerRecord record;

    public Player()
    {
    }

    public Player(int id, String fullName, UriBuilder builder)
    {
        this.id = id;
        this.fullName = fullName;
        this.link = Link.self(builder.build(this.id));
    }

    public Player(int id, UriBuilder builder)
    {
        this.id = id;
        this.link = Link.self(builder.build(this.id));
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setRecord(PlayerRecord record)
    {
        this.record = record;
    }

    public PlayerRecord getRecord()
    {
        return record;
    }
}