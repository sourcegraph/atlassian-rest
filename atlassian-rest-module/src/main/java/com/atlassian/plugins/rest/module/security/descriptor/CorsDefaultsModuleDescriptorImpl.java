package com.atlassian.plugins.rest.module.security.descriptor;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;

/**
 * Basic module descriptor for Cross-Origin Resource Sharing default instances
 */
public final class CorsDefaultsModuleDescriptorImpl extends AbstractModuleDescriptor<CorsDefaults> implements CorsDefaultsModuleDescriptor
{
    public CorsDefaultsModuleDescriptorImpl(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public CorsDefaults getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }
}
