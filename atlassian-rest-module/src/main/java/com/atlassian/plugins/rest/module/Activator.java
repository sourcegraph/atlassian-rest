package com.atlassian.plugins.rest.module;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Activator implements InitializingBean, DisposableBean
{
    private final BundleActivator coreActivator = new com.sun.jersey.core.osgi.Activator();
    private final BundleActivator serverActivator = new com.sun.jersey.server.osgi.Activator();
    private final BundleContext bundleContext;

    public Activator(BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
    }

    public void afterPropertiesSet() throws Exception
    {
        coreActivator.start(bundleContext);
        serverActivator.start(bundleContext);
    }

    public void destroy() throws Exception
    {
        serverActivator.stop(bundleContext);
        coreActivator.stop(bundleContext);
    }
}
