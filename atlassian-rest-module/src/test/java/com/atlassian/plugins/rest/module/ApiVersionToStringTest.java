package com.atlassian.plugins.rest.module;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ApiVersionToStringTest
{
    @Test
    public void dottedVersionIsCorrectlyDescribed()
    {
        assertThat(new ApiVersion("1.2.3").toString(), is("1.2.3"));
    }

    @Test
    public void integerVersionIsCorrectlyDescribed()
    {
        assertThat(new ApiVersion("1").toString(), is("1"));
    }

    @Test
    public void noneVersionIsCorrectlyDescribed()
    {
        assertThat(new ApiVersion(ApiVersion.NONE_STRING).toString(), is(ApiVersion.NONE_STRING));
    }
}
