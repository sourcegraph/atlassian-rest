package com.atlassian.plugins.rest.common.security.jersey.test;

import com.atlassian.plugins.rest.common.security.CorsAllowed;

@CorsAllowed
public class ResourceClassWithCorsAllowed
{
}
