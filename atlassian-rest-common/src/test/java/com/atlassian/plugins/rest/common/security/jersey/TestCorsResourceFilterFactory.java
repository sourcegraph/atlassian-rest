package com.atlassian.plugins.rest.common.security.jersey;

import java.lang.annotation.Annotation;
import java.util.List;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugins.rest.common.security.CorsAllowed;
import com.atlassian.plugins.rest.common.security.jersey.test.ResourceClassWithCorsAllowed;
import com.atlassian.plugins.rest.common.security.jersey.test.ResourceClassWithoutCorsAllowed;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.spi.container.ResourceFilter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCorsResourceFilterFactory
{
    @Mock
    private PluginEventManager pluginEventManager;

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private AbstractMethod jerseyMethod;

    private CorsResourceFilterFactory corsResourceFilterFactory;

    private static final Annotation[] EMPTY_ANNOTATIONS_ARRAY = new Annotation[0];

    @Before
    public void setup()
    {
        corsResourceFilterFactory = new CorsResourceFilterFactory(pluginAccessor, pluginEventManager);
    }

    @Test
    public void testThatCorsAllowedIsDetectedOnMethod()
    {
        when(jerseyMethod.isAnnotationPresent(CorsAllowed.class)).thenReturn(true);
        when(jerseyMethod.getAnnotations()).thenReturn(EMPTY_ANNOTATIONS_ARRAY);

        List<ResourceFilter> resourceFilters = corsResourceFilterFactory.create(jerseyMethod);

        assertContainsASingleCorsResourceFilter(resourceFilters);
    }

    @Test
    public void testThatCorsAllowedIsDetectedOnJerseyResource()
    {
        when(jerseyMethod.getResource()).thenReturn(new AbstractResource(ResourceClassWithCorsAllowed.class));
        when(jerseyMethod.getAnnotations()).thenReturn(EMPTY_ANNOTATIONS_ARRAY);

        List<ResourceFilter> resourceFilters = corsResourceFilterFactory.create(jerseyMethod);

        assertContainsASingleCorsResourceFilter(resourceFilters);
    }

    @Test
    public void testThatCorsAllowedIsDetectedOnResourcePackage() throws ClassNotFoundException
    {
        when(jerseyMethod.getResource()).thenReturn(new AbstractResource(ResourceClassWithoutCorsAllowed.class));
        when(jerseyMethod.getAnnotations()).thenReturn(EMPTY_ANNOTATIONS_ARRAY);

        List<ResourceFilter> resourceFilters = corsResourceFilterFactory.create(jerseyMethod);

        assertContainsASingleCorsResourceFilter(resourceFilters);
    }

    @Test
    public void testThatNoCorsAllowedAnnotationsResultsInEmptyFilters() throws ClassNotFoundException
    {
        when(jerseyMethod.getResource()).thenReturn(new AbstractResource(this.getClass()));

        List<ResourceFilter> resourceFilters = corsResourceFilterFactory.create(jerseyMethod);
        assertThat(resourceFilters, is(empty()));
    }

    @Test
    public void testThatResourceInDefaultPackageIsTolerated() throws ClassNotFoundException
    {
        when(jerseyMethod.getResource()).thenReturn(new AbstractResource(Class.forName("ClassInDefaultPackageForTesting")));

        List<ResourceFilter> resourceFilters = corsResourceFilterFactory.create(jerseyMethod);
        assertThat(resourceFilters.size(), is(0));
    }

    private static void assertContainsASingleCorsResourceFilter(List<ResourceFilter> resourceFilters)
    {
        assertThat(resourceFilters.size(), is(1));
        assertThat(resourceFilters.get(0), is(instanceOf(CorsResourceFilter.class)));
    }
}