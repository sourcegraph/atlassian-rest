package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 */
@RunWith (MockitoJUnitRunner.class)
public class TestXsrfResourceFilter
{
    private XsrfResourceFilter xsrfResourceFilter;
    @Mock
    private ContainerRequest request;
    @Mock
    private HttpContext httpContext;
    @Mock
    private HttpServletRequest httpServletRequest;

    @Before
    public void setUp()
    {
        xsrfResourceFilter = new XsrfResourceFilter();
        xsrfResourceFilter.setXsrfRequestValidator(
            new XsrfRequestValidatorImpl(
                new IndependentXsrfTokenValidator(
                    new IndependentXsrfTokenAccessor())));
        when(httpContext.getRequest()).thenReturn(httpServletRequest);
        xsrfResourceFilter.setHttpContext(httpContext);
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testGetProtectedByXsrfChecks()
    {
        when(request.getMethod()).thenReturn("GET");
        addTokenHeaderToRequest(httpServletRequest, "invalid");
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostBlocked()
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void testPostSuccessWithCorrectHeaderValue()
    {
        when(request.getMediaType()).thenReturn(
            MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(request.getMethod()).thenReturn("POST");
        addTokenHeaderToRequest(httpServletRequest,
            XsrfResourceFilter.NO_CHECK);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPostSuccessWithOldHeaderValue()
    {
        final String deprecatedHeaderValue = "nocheck";
        when(request.getMediaType()).thenReturn(
            MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(request.getMethod()).thenReturn("POST");
        when(request.getHeaderValue(XsrfResourceFilter.TOKEN_HEADER)).
            thenReturn(deprecatedHeaderValue);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void tesPostFailureWithInvalidHeaderValue()
    {
        when(request.getMediaType()).thenReturn(
            MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(request.getMethod()).thenReturn("POST");
        addTokenHeaderToRequest(httpServletRequest, "invalid");
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPostJsonSuccess()
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_JSON_TYPE);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPutFormSuccess()
    {
        when(request.getMethod()).thenReturn("PUT");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPostWithValidXsrfTokenSuccess()
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        for (String validToken : Arrays.asList("abc123", "ffff", "FFFx3~324", "☃☃☃"))
        {
            httpServletRequest = setupRequestWithXsrfToken(httpServletRequest, validToken, validToken);
            assertThat(xsrfResourceFilter.filter(request), is(request));
        }
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostWithInvalidXsrfTokenBlocked()
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        httpServletRequest = setupRequestWithXsrfToken(httpServletRequest, "abc123", "notabc123");
        xsrfResourceFilter.filter(request);
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostWithNullHttpServletRequestXsrfTokenBlocked()
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(
            MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(httpContext.getRequest()).thenReturn(null);
        xsrfResourceFilter.setHttpContext(httpContext);
        xsrfResourceFilter.filter(request);
    }

    private void addTokenHeaderToRequest(
        HttpServletRequest httpServletRequest, String headerValue)
    {
        when(httpServletRequest.getHeader(XsrfResourceFilter.TOKEN_HEADER))
            .thenReturn(headerValue);
    }

    private static HttpServletRequest setupRequestWithXsrfToken(
        HttpServletRequest request, String expectedToken, String formToken)
    {
        Cookie [] cookies = {new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, expectedToken)};
        when(request.getCookies()).thenReturn(cookies);
        when(request.getParameter(IndependentXsrfTokenValidator.XSRF_PARAM_NAME)).thenReturn(formToken);
        return request;
    }
}
