package com.atlassian.plugins.rest.common.validation;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a validation error
 *
 * @since 2.0
 */
@XmlRootElement
@XmlAccessorType
public class ValidationError
{
    private String message;
    private String path;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }
}
