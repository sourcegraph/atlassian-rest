package com.atlassian.plugins.rest.common.error.jersey;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.common.Status;

/**
 * An analog of {@link Status} specifically for uncaught exceptions. The stack trace is
 * included.
 */
@XmlRootElement(name="status")
public class UncaughtExceptionEntity
{
    private static final Integer INTERNAL_SERVER_ERROR_CODE =
            Integer.valueOf(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());

    /**
     * This is a redundant duplicate of the HTTP error code but it may be clearer in
     * a browser.
     */
    @XmlElement(name = "status-code")
    private final Integer code = INTERNAL_SERVER_ERROR_CODE;

    /**
     * A humane readable message for the given status.
     */
    @XmlElement
    private final String message;

    @XmlElement(name = "stack-trace")
    private final String stackTrace;

    /**
     * JAXB requires a default constructor.
     */
    public UncaughtExceptionEntity()
    {
        this.message = null;
        this.stackTrace = "";
    }

    public UncaughtExceptionEntity(Throwable t)
    {
        this.message = t.getMessage();

        StringWriter sw = new StringWriter();

        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);

        this.stackTrace = sw.toString();
    }

    public String getMessage()
    {
        return message;
    }

    public String getStackTrace()
    {
        return stackTrace;
    }

    private static final MediaType TEXT_PLAIN_UTF8_TYPE = MediaType.valueOf("text/plain; charset=utf-8");

    /**
     * These are the media types that an UncaughtExceptionEntity can be represented as.
     */
    private static final List<Variant> POSSIBLE_VARIANTS = Variant.mediaTypes(
            MediaType.APPLICATION_XML_TYPE,
            MediaType.APPLICATION_JSON_TYPE,
            MediaType.TEXT_PLAIN_TYPE).add().build();

    public static MediaType variantFor(Request request)
    {
        Variant v = request.selectVariant(POSSIBLE_VARIANTS);
        if (v == null)
        {
            v = POSSIBLE_VARIANTS.get(0);
        }

        /* If we include the charset in the variant then it gets prioritised as a default. Select
         * it as text/plain and then switch in the variant with the charset here.
         */
        MediaType t = v.getMediaType();

        if (t.equals(MediaType.TEXT_PLAIN_TYPE))
        {
            return TEXT_PLAIN_UTF8_TYPE;
        }
        else
        {
            return t;
        }
    }
}
