package com.atlassian.plugins.rest.common.error.jersey;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A generic exception mapper that will map any {@link Throwable throwable}.  Handles the special case of
 * {@link WebApplicationException}, which provides its own response.
 *
 * @since 1.0
 */
@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable>
{
    private static final Logger log = LoggerFactory.getLogger(ThrowableExceptionMapper.class);

    @Context
    Request request;

    public Response toResponse(Throwable t)
    {
        if (t instanceof WebApplicationException)
        {
            // Internal Server Error: a bug or other error -> log it with a stack trace just in case it wasn't logged already
            // Note that other 5xx codes are specific well-known codes that do not indicate a potential bug.
            // For more details see: https://bitbucket.org/atlassian/atlassian-rest/pull-request/12#comment-1088837

            final WebApplicationException webEx = (WebApplicationException) t;
            if (webEx.getResponse().getStatus() == Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            {
                log.error("Server Error in REST: " + webEx.getResponse().getStatus() + ": " + webEx.getResponse(), t);
            }
            else
            {
                log.debug("REST response: {}: {}", webEx.getResponse().getStatus(), webEx.getResponse());
            }
            return webEx.getResponse();
        }
        else
        {
            log.error("Uncaught exception thrown by REST service: " + t.getMessage(), t);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new UncaughtExceptionEntity(t))
                    .type(UncaughtExceptionEntity.variantFor(request))
                    .build();
        }
    }
}
