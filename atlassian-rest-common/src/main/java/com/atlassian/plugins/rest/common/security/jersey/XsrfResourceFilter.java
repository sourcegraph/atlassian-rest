package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;


/**
 * A filter that filters requests that need XSRF protection.
 * <p>
 * This checks for the presence of the no-check xsrf header and if the xsrf token is correct.
 *
 * @since 2.4
 */
public class XsrfResourceFilter implements ResourceFilter, ContainerRequestFilter
{
    public static final String TOKEN_HEADER = "X-Atlassian-Token";
    public static final String NO_CHECK = "no-check";

    private HttpContext httpContext;
    private XsrfRequestValidator xsrfRequestValidator;
    private Response.Status failureStatus = Response.Status.FORBIDDEN;

    private static final Logger log = LoggerFactory.getLogger(
        XsrfResourceFilter.class);

    private static final Set<String> XSRFABLE_TYPES = new HashSet<String>(Arrays.asList(
            MediaType.APPLICATION_FORM_URLENCODED, MediaType.MULTIPART_FORM_DATA, MediaType.TEXT_PLAIN
    ));


    public void setHttpContext(HttpContext httpContext)
    {
        this.httpContext = httpContext;
    }

    public void setXsrfRequestValidator(XsrfRequestValidator xsrfRequestValidator)
    {
        this.xsrfRequestValidator = xsrfRequestValidator;
    }

    public void setFailureStatus(Response.Status failureStatus)
    {
        if (!(failureStatus == Response.Status.FORBIDDEN ||
            (failureStatus == Response.Status.NOT_FOUND)))
        {
            throw new IllegalArgumentException(
                "Only FORBIDDEN and NOT_FOUND status are valid arguments.");
        }
        this.failureStatus = failureStatus;
    }

    public ContainerRequest filter(final ContainerRequest request)
    {
        if (!isXsrfable(request))
        {
            return request;
        }
        HttpServletRequest httpServletRequest = null;
        if (httpContext != null)
        {
            httpServletRequest = this.httpContext.getRequest();
        }
        if (httpServletRequest == null || !(xsrfRequestValidator.
            validateRequestPassesXsrfChecks(httpServletRequest) ||
            hasDeprecatedHeaderValue(request)))
        {
            throw new XsrfCheckFailedException(failureStatus);
        }
        return request;
    }

    private boolean hasDeprecatedHeaderValue(ContainerRequest request)
    {
        final String header = request.getHeaderValue(TOKEN_HEADER);
        if (header == null)
        {
            return false;
        }
        if (header.toLowerCase(Locale.ENGLISH).equals("nocheck"))
        {
            log.warn("Use of the 'nocheck' value for {} " +
                "has been deprecated since rest 3.0.0. Please use a value of " +
                "'no-check' instead.", TOKEN_HEADER);
            return true;
        }
        return false;
    }

    private boolean isXsrfable(ContainerRequest request)
    {
        String method = request.getMethod();
        return method.equals("GET") || (
            method.equals("POST") && (request.getMediaType() == null
            || XSRFABLE_TYPES.contains(mediaTypeToString(request.getMediaType()))));
    }

    public ContainerRequestFilter getRequestFilter()
    {
        return this;
    }

    public ContainerResponseFilter getResponseFilter()
    {
        return null;
    }

    private static String mediaTypeToString(MediaType mediaType)
    {
        return mediaType.getType().toLowerCase(Locale.ENGLISH) + "/" + mediaType.getSubtype().toLowerCase(Locale.ENGLISH);
    }

}
