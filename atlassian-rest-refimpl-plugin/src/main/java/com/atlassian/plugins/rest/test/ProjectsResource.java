package com.atlassian.plugins.rest.test;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import static javax.ws.rs.core.MediaType.*;
import javax.ws.rs.core.UriInfo;

@Path("/projects")
@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
public class ProjectsResource
{
    private final Projects projects = new Projects();

    @GET
    public Projects getAllProjects() throws Exception
    {
        return projects;
    }

    @Path("{project}")
    public ProjectSubResource getProject(@PathParam("project") String projectName, @Context UriInfo uriInfo)
    {
        return new ProjectSubResource(projects, projectName, uriInfo);
    }
}
