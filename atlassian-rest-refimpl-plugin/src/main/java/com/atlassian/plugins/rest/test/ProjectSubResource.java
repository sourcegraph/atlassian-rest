package com.atlassian.plugins.rest.test;

import com.google.common.base.Preconditions;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
public class ProjectSubResource
{
    private final Project project;
    private final UriInfo uriInfo;

    public ProjectSubResource(Projects projects, String projectName, UriInfo uriInfo)
    {
        this.project = projects.getProjectsMap().get(projectName);
        this.uriInfo = Preconditions.checkNotNull(uriInfo);
    }

    @GET
    public Response getProject()
    {
        return project != null ? Response.ok(project.build(uriInfo)).build() : Response.status(Response.Status.NOT_FOUND).build();
    }
}