package com.atlassian.plugins.rest.cors;

import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * A basic implementation of {@link CorsDefaults} purely for testing purposes. This is not published by the REST module.
 */
public class SimpleCorsDefaults implements CorsDefaults
{
    public static final String CREDENTIALS = "http://credentials.test.com";
    public static final String NO_CREDENTIALS = "http://nocredentials.test.com";
    
    private static final Set<String> WHITELIST = ImmutableSet.of(CREDENTIALS, NO_CREDENTIALS);

    public boolean allowsCredentials(String uri) throws IllegalArgumentException
    {
        return CREDENTIALS.equals(uri);
    }

    public boolean allowsOrigin(String uri)
    {
        return WHITELIST.contains(uri);
    }

    public Set<String> getAllowedRequestHeaders(String uri)
    {
        return ImmutableSet.of("X-Custom-Header");
    }

    public Set<String> getAllowedResponseHeaders(String uri)
    {
        return ImmutableSet.of("X-Response-Header");
    }
}
