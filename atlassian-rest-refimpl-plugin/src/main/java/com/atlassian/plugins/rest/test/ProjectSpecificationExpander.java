package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.AbstractRecursiveEntityExpander;

public class ProjectSpecificationExpander extends AbstractRecursiveEntityExpander<ProjectSpecification>
{
    protected ProjectSpecification expandInternal(ProjectSpecification specification)
    {
        specification.setTitle("Specification Title");
        specification.setText("Specification Text");
        return specification;
    }
}
