package com.atlassian.plugins.rest.multipart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FilePartObject
{
    @XmlAttribute
    private String fieldName;

    @XmlAttribute
    private boolean field;

    @XmlAttribute
    private String filename;

    @XmlElement
    private String value;

    @XmlAttribute
    private String contentType;

    public FilePartObject()
    {
    }

    public FilePartObject(final String fieldName, final boolean field, final String filename, final String contentType, final String value)
    {
        this.fieldName = fieldName;
        this.field = field;
        this.filename = filename;
        this.value = value;
        this.contentType = contentType;
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public void setFieldName(final String fieldName)
    {
        this.fieldName = fieldName;
    }

    public boolean isField()
    {
        return field;
    }

    public void setField(final boolean field)
    {
        this.field = field;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(final String filename)
    {
        this.filename = filename;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(final String contentType)
    {
        this.contentType = contentType;
    }
}
