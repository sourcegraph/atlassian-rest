package it.com.atlassian.rest.json;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION;
import static it.com.atlassian.rest.test.ProjectsTest.JSONP_CALLBACK_PARAMETER_NAME;
import static it.com.atlassian.rest.test.ProjectsTest.JSONP_CALLBACK_TEST_FUNCTION_NAME;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 * Json with Padding must be DISABLED in the REST plugin module for these tests to pass.
 * Currently this is done by setting the following system property {@link com.atlassian.plugins.rest.module.json.JsonWithPaddingResponseFilter#ATLASSIAN_ALLOW_JSONP}
 * to 'true'
 */
public class JsonWithPaddingDisabledTest
{
    private WebResource webResourceWithVersion;

    @Before
    public void setUp()
    {
        webResourceWithVersion = WebResourceFactory.authenticated(REST_VERSION).path("projects");
    }

    @Test
    public void testGetProjectsWithJsonDoesNotContainJsonpCallbackWhenJsonPDisabled()
    {
        final String projectsJson = webResourceWithVersion.accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);

        final MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
        queryParams.putSingle(JSONP_CALLBACK_PARAMETER_NAME, JSONP_CALLBACK_TEST_FUNCTION_NAME);

        final String projectsJsonP = webResourceWithVersion.queryParams(queryParams).accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);
        assertThat(projectsJsonP, not(containsString(JSONP_CALLBACK_TEST_FUNCTION_NAME)));

        assertThat(projectsJsonP, equalTo(projectsJson));
    }

}
