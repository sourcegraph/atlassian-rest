package it.com.atlassian.rest.test;

import com.atlassian.plugins.rest.module.json.JsonWithPaddingResponseFilter;
import com.atlassian.plugins.rest.test.Developer;
import com.atlassian.plugins.rest.test.Project;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION;

import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

public class ProjectsTest
{
    public static final String PROJECT_1 = "project1";

    public static final String JSONP_CALLBACK_PARAMETER_NAME = "jsonp-callback";
    public static final String JSONP_CALLBACK_TEST_FUNCTION_NAME = "myfunction";

    private WebResource webResourceWithVersion;

    @Before
    public void setUp()
    {
        webResourceWithVersion = WebResourceFactory.authenticated(REST_VERSION).path("projects");
    }

    @Test
    public void testGetProjectByNameAndExpanding()
    {
        Project project = webResourceWithVersion.path(PROJECT_1).get(Project.class);
        assertEquals(PROJECT_1, project.getName());
        assertNotNull(project.getInformation().getLink());
        assertNull(project.getInformation().getLongName());
        assertNull(project.getInformation().getLongDescription());

        testGetProjectExpandingInformation(webResourceWithVersion, "information");
        testGetProjectExpandingInformation(webResourceWithVersion, "*");

        testGetProjectExpandingInformationAndSpecification(webResourceWithVersion, "information.specification");
        testGetProjectExpandingInformationAndSpecification(webResourceWithVersion, "information.*");
        testGetProjectExpandingInformationAndSpecification(webResourceWithVersion, "*.*");

        testGetProjectExpandingDevelopersAndFavouriteDrinks(webResourceWithVersion, "developers.developer.drinks");
    }

    @Test
    public void testGetProjectExpandingDevelopers()
    {
        testGetProjectExpandingDevelopersWithIndex(webResourceWithVersion);
        testGetProjectExpandingAllDevelopers(webResourceWithVersion);
    }

    @Test
    public void testGetProjectExpandingInformationAndDevelopers()
    {
        final Project project = webResourceWithVersion.path(PROJECT_1).queryParam("expand", "information,developers").get(Project.class);

        assertEquals(PROJECT_1, project.getName());


        // information expanded
        assertNotNull(project.getInformation().getLink());
        assertNotNull(project.getInformation().getLongName());
        assertNotNull(project.getInformation().getLongDescription());

        // developers list expanded
        assertEquals(6, project.getDevelopers().size());
    }

    @Test
    public void testGetProjectExpandingDevelopersRange()
    {
        String index = "1:3";
        Project project = webResourceWithVersion.path("project1").queryParam("expand", "developers[" + index + "]").get(Project.class);


        assertEquals(3, project.getDevelopers().size());
        assertEquals("developer2", project.getDevelopers().get(0).getUserName());
        assertEquals("developer3", project.getDevelopers().get(1).getUserName());
        assertEquals("developer4", project.getDevelopers().get(2).getUserName());

        index = "-3:-1";
        project = webResourceWithVersion.path("project1").queryParam("expand", "developers[" + index + "]").get(Project.class);

        assertEquals(3, project.getDevelopers().size());
        assertEquals("developer4", project.getDevelopers().get(0).getUserName());
        assertEquals("developer5", project.getDevelopers().get(1).getUserName());
        assertEquals("developer6", project.getDevelopers().get(2).getUserName());
    }

    private void testGetProjectExpandingAllDevelopers(WebResource webResource)
    {
        Project project = webResource.path(PROJECT_1).queryParam("expand", "developers").get(Project.class);
        assertEquals(6, project.getDevelopers().size());

        for (Developer developer : project.getDevelopers())
        {
            assertNotNull(developer.getLink());
            assertNotNull(developer.getUserName());
            assertNull(developer.getFullName());
            assertNull(developer.getEmail());
        }

        project = webResource.path(PROJECT_1).queryParam("expand", "developers.developer").get(Project.class);
        assertEquals(6, project.getDevelopers().size());

        for (Developer developer : project.getDevelopers())
        {
            assertNotNull(developer.getLink());
            assertNotNull(developer.getUserName());
            assertNotNull(developer.getFullName());
            assertNotNull(developer.getEmail());
        }
    }

    private void testGetProjectExpandingDevelopersWithIndex(WebResource webResource)
    {
        testGetProjectExpandingDevelopersWithIndex(webResource, 0);
        testGetProjectExpandingDevelopersWithIndex(webResource, 1);
        testGetProjectExpandingDevelopersWithIndex(webResource, 2);
        testGetProjectExpandingDevelopersWithIndex(webResource, 3);
        testGetProjectExpandingDevelopersWithIndex(webResource, 4);
        testGetProjectExpandingDevelopersWithIndex(webResource, 5);
        testGetProjectExpandingDevelopersWithIndex(webResource, 6);
    }

    private void testGetProjectExpandingDevelopersWithIndex(WebResource webResource, int index)
    {
        String expand = "developers[" + index + "]";
        Project project = webResource.path("project1").queryParam("expand", expand).get(Project.class);

        if (index >= 0 && index < 6)
        {

            assertEquals(1, project.getDevelopers().size());
            Developer developer = project.getDevelopers().get(0);

            assertNotNull(developer.getLink());
            assertNotNull(developer.getUserName());
            assertNull(developer.getFullName());
            assertNull(developer.getEmail());

            expand += ".developer";
            project = webResource.path("project1").queryParam("expand", expand).get(Project.class);

            assertEquals(1, project.getDevelopers().size());
            developer = project.getDevelopers().get(0);

            assertNotNull(developer.getLink());
            assertNotNull(developer.getUserName());
            assertNotNull(developer.getFullName());
            assertNotNull(developer.getEmail());
        }
        else
        {
            assertTrue(project.getDevelopers().isEmpty());
        }
    }

    private void testGetProjectExpandingInformation(WebResource webResource, final String expand)
    {
        Project project;// expanding the information attibute
        project = webResource.path(PROJECT_1).queryParam("expand", expand).get(Project.class);
        assertEquals(PROJECT_1, project.getName());
        assertNotNull(project.getInformation().getLink());
        assertNotNull(project.getInformation().getLongName());
        assertNotNull(project.getInformation().getLongDescription());

        assertNotNull(project.getInformation().getSpecification().getLink());
        assertNull(project.getInformation().getSpecification().getTitle());
        assertNull(project.getInformation().getSpecification().getText());
    }

    private void testGetProjectExpandingInformationAndSpecification(WebResource webResource, final String expand)
    {
        Project project = webResource.path(PROJECT_1).queryParam("expand", expand).get(Project.class);
        assertEquals(PROJECT_1, project.getName());
        assertNotNull(project.getInformation().getLink());
        assertNotNull(project.getInformation().getLongName());
        assertNotNull(project.getInformation().getLongDescription());

        assertNotNull(project.getInformation().getSpecification().getLink());
        assertNotNull(project.getInformation().getSpecification().getTitle());
        assertNotNull(project.getInformation().getSpecification().getText());
    }

    public void testGetProjectExpandingDevelopersAndFavouriteDrinks(WebResource webResource, final String expand)
    {
        Project project = webResource.path(PROJECT_1).queryParam("expand", expand).get(Project.class);
        assertEquals(PROJECT_1, project.getName());
        assertTrue(project.getDevelopers().get(0).getFavouriteDrinks().size() > 0);
    }
}
