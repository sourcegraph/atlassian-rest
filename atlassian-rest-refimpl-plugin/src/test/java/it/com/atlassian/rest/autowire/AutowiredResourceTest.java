package it.com.atlassian.rest.autowire;

import com.atlassian.plugins.rest.autowiring.SomeService;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AutowiredResourceTest
{
    private static final String PLUGIN_SERVICE_NAME = "Plugin service instance";

    private WebResource webResource;

    @Before
    public void setUp()
    {
        webResource = WebResourceFactory.authenticated();
    }

    @Test
    public void testGetAutowiredResource()
    {
        final SomeService someService = webResource.path("autowired").get(SomeService.class);

        assertEquals(PLUGIN_SERVICE_NAME, someService.getName());
    }
}
